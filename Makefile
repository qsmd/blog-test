# the directory where existing assets used in this Makefile are stored
# calling this "assets" would clash with Hugo's assets folder
MAKE_ASSETS_DIR ?= make-assets

# the directory that needs to be added to the user's PATH
# calling this "bin" would clash with Pandoc's extraction folder
MAKE_BIN_DIR ?= make-bin

# Hugo's content directory
CONTENT_DIR ?= content

# link *.md to *.LANG.md
LN_LANG ?= de
LN_SRC=$(shell find $(CONTENT_DIR) -type f -name '*.$(LN_LANG).md')
LN_DEST=$(LN_SRC:%.$(LN_LANG).md=%.md)

.PHONY: pandoc hugo gemini default clean
.PRECIOUS: $(MAKE_BIN_DIR)/. $(MAKE_ASSETS_DIR)/. ~/.local/share%/. layouts/. layouts%/. assets/. assets%/.

pandoc: $(MAKE_BIN_DIR)/pandoc $(MAKE_BIN_DIR)/pandoc-default ~/.local/share/pandoc/filters/img-lazy-loading.lua ~/.local/share/pandoc/filters/img-add-size.lua ~/.local/share/pandoc/filters/link-rel.lua

hugo: layouts/partials/header.html layouts/partials/footer.html layouts/partials/pagedjs-header.html static/admin/reset.css assets/css/extended/highlighting.css

gemini: $(MAKE_BIN_DIR)/gmnhg $(MAKE_BIN_DIR)/md2gmn

gemini-content: $(LN_DEST)

clean:
	rm -f $(MAKE_BIN_DIR)/pandoc $(MAKE_BIN_DIR)/pandoc-default
	[ "$$(ls -A $(MAKE_BIN_DIR)/)" ] && echo "not empty" || rm -rf $(MAKE_BIN_DIR)/
	rm -f ~/.local/share/pandoc/filters/img-lazy-loading.lua
	rm -f ~/.local/share/pandoc/filters/img-add-size.lua
	rm -f ~/.local/share/pandoc/filters/link-rel.lua
	[ "$$(ls -A ~/.local/share/pandoc/filters/)" ] && echo "not empty" || rm -rf ~/.local/share/pandoc/filters/
	[ "$$(ls -A ~/.local/share/pandoc/)" ] && echo "not empty" || rm -rf ~/.local/share/pandoc/
	rm -f ./layouts/partials/header.html ./layouts/partials/footer.html ./layouts/partials/pagedjs-header.html
	[ "$$(ls -A ./layouts/partials/)" ] && echo "not empty" || rm -rf ./layouts/partials/
	[ "$$(ls -A ./layouts/)" ] && echo "not empty" || rm -rf ./layouts/
	rm -f static/admin/reset.css
	rm -f assets/css/extended/highlighting.css
	[ "$$(ls -A ./assets/css/extended/)" ] && echo "not empty" || rm -rf ./assets/css/extended/
	[ "$$(ls -A ./assets/css/)" ] && echo "not empty" || rm -rf ./assets/css/
	[ "$$(ls -A ./assets/)" ] && echo "not empty" || rm -rf ./assets/

# for automatic directory creation
# see https://ismail.badawi.io/blog/2017/03/28/automatic-directory-creation-in-make/
$(MAKE_BIN_DIR)/.:
	mkdir -p $@

$(MAKE_ASSETS_DIR)/.:
	mkdir -p $@

~/.local/share%/.:
	mkdir -p $@

layouts/.:
	mkdir -p $@

layouts%/.:
	mkdir -p $@

assets/.:
	mkdir -p $@

assets%/.:
	mkdir -p $@

.SECONDEXPANSION:

$(MAKE_BIN_DIR)/pandoc: $(MAKE_ASSETS_DIR)/pandoc | $$(@D)/.
	cp $< $@
	chmod +x $@

$(MAKE_BIN_DIR)/pandoc-default: | $$(@D)/.
	curl -sL https://api.github.com/repos/jgm/pandoc/releases/latest | jq -r ".assets[].browser_download_url | select (. | contains(\"tar.gz\"))" | xargs -n 1 curl -sSL | tar -xz --strip-components=1 --wildcards "*/bin"
	mv ./bin/pandoc $@
	[ "$$(ls -A ./bin/)" ] && echo "not empty" || rm -rf ./bin/
	chmod +x $@

~/.local/share/pandoc/filters/img-lazy-loading.lua: $(MAKE_ASSETS_DIR)/img-lazy-loading.lua | $$(@D)/.
	cp $< $@

~/.local/share/pandoc/filters/img-add-size.lua: $(MAKE_ASSETS_DIR)/img-add-size.lua | $$(@D)/.
	cp $< $@

~/.local/share/pandoc/filters/link-rel.lua: $(MAKE_ASSETS_DIR)/link-rel.lua | $$(@D)/.
	cp $< $@

layouts/partials/header.html: $(MAKE_ASSETS_DIR)/header.patch | $$(@D)/.
	patch --forward --reject-file=- --no-backup-if-mismatch --output=$@ ./themes/PaperMod/layouts/partials/header.html $<

layouts/partials/footer.html: $(MAKE_ASSETS_DIR)/footer.patch | $$(@D)/.
	patch --forward --reject-file=- --no-backup-if-mismatch --output=$@ ./themes/PaperMod/layouts/partials/footer.html $<

layouts/partials/pagedjs-header.html: $(MAKE_ASSETS_DIR)/pagedjs-header.patch | $$(@D)/.
	patch --forward --reject-file=- --no-backup-if-mismatch --output=$@ ./themes/pagedjs/layouts/partials/pagedjs-header.html $<

static/admin/reset.css: themes/PaperMod/assets/css/reset.css
	cp $< $@

assets/css/extended/highlighting.css: $(MAKE_BIN_DIR)/pandoc-default | $$(@D)/.
	template_file=$$(mktemp) && \
	echo '$$highlighting-css$$' > $$template_file && \
	input_file=$$(mktemp) && \
	echo '~~~html' > $$input_file && \
	echo '~~~' >> $$input_file && \
	$(MAKE_BIN_DIR)/pandoc-default --from=markdown --to=html5 --highlight-style=zenburn --template=$$template_file --output=$@ $$input_file && \
	rm $$template_file && \
	rm $$input_file

$(MAKE_BIN_DIR)/gmnhg $(MAKE_BIN_DIR)/md2gmn: | $$(@D)/.
	curl -sSL https://git.tdem.in/attachments/7785bc4c-ef23-454f-9569-46fccaaa54df | tar -xz -C $(MAKE_BIN_DIR)
	rm $(MAKE_BIN_DIR)/README.md

$(CONTENT_DIR)/%.md: $(CONTENT_DIR)/%.$(LN_LANG).md
	ln $< $@

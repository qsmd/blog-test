---
title: Colophon
slug: colophon
menu: main
weight: 3
lastmod: ""
---
This site is largely powered by [free/libre software](https://www.gnu.org/philosophy/free-sw.html):

- [Netlify CMS](https://www.netlifycms.org/) is used for everyday authoring in markdown format
- [Hugo](https://gohugo.io/) and [Pandoc](https://pandoc.org/) are used to create a static website
  - [PaperMod](https://github.com/adityatelange/hugo-PaperMod/) is used as theme
  - [Paged.js](https://gitlab.pagedmedia.org/julientaq/pagedjs-hugo) is used for pretty printing
- [GitLab](https://about.gitlab.com/) running at [Framagit](https://framagit.org/) is used to host the source code and the website
- Icons: [Feather](https://feathericons.com/) (MIT/Expat) and [Logobridge](https://logobridge.co/) ([CC0](https://creativecommons.org/publicdomain/zero/1.0/))

All content on this site is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/) (or later). The source code can be accessed via the following link: [`framagit.org/qsmd/blog`](https://framagit.org/qsmd/blog). No trackers or cookies are being used. Changing the color scheme will save one's choice in the browser, but this can [easily be deleted again](https://support.mozilla.org/kb/storage).
